import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SucursalesModule } from './sucursales/sucursales.module';
import { NoautorizadoComponent } from './noautorizado/noautorizado.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    NoautorizadoComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SucursalesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
