import { Component, OnInit } from '@angular/core';
import { SessionService } from '../core/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private session: SessionService, private router: Router) { }

  ngOnInit() {
    this.session.validarLogin()
      .then(() => this.router.navigateByUrl(""));
  }

}
