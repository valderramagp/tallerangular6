import { SucursalesComponent } from "./sucursales/sucursales.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from "./core/auth-guard.service";
import { NoautorizadoComponent } from "./noautorizado/noautorizado.component";
import { LoginComponent } from "./login/login.component";

const routes: Routes = [
	{
		path: "",
		redirectTo: "sucursal",
		pathMatch: "full"
	},
	{
		path: "sucursal",
		component: SucursalesComponent,
		canActivate: [
			AuthGuardService
		]
	},
	{
		path: "noautorizado",
		component: NoautorizadoComponent
	},
	{
		path: "login",
		component: LoginComponent
	}
]

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule {}