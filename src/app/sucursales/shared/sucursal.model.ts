export class Sucursal {
	id: number;
	nombre: string;
	email: string;
	cp: string;
}