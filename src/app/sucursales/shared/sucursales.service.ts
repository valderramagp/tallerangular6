import { Injectable } from '@angular/core';
import { ApiService } from '../../core/api.service';
import { environment } from '../../../environments/environment';
import { SessionService } from '../../core/session.service';
import { Sucursal } from './sucursal.model';

@Injectable()
export class SucursalesService {
  private endpoint: string;

  constructor(private api: ApiService, session: SessionService) { 
    this.endpoint = `api/v1/emisores/${session.emisorId}/sucursales`;
  }

  obtenerSucursales() {
    return this.api.obtener<Sucursal[]>(environment.apiEmision, this.endpoint);
  }
}
