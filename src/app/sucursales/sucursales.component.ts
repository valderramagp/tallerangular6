import { Component, OnInit } from '@angular/core';
import { SessionService } from '../core/session.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Sucursal } from './shared/sucursal.model';
import { SucursalesService } from './shared/sucursales.service';

@Component({
  selector: 'app-sucursales',
  templateUrl: './sucursales.component.html',
  styleUrls: ['./sucursales.component.css']
})
export class SucursalesComponent implements OnInit {
  sucursales: Observable<Sucursal[]>;

  constructor(private sucursalesService: SucursalesService) { }

  ngOnInit() {
    this.sucursales = this.sucursalesService.obtenerSucursales().pipe(map(sucursales => {
      sucursales.forEach(sucursal => {
        sucursal.nombre = "Hola";
      });
      return sucursales;
    }));
  }

}
