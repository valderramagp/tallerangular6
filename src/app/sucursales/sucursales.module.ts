import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SucursalesComponent } from './sucursales.component';
import { SucursalesService } from './shared/sucursales.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SucursalesComponent],
  providers: [
    SucursalesService
  ]
})
export class SucursalesModule { }
