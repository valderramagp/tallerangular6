import { Injectable } from '@angular/core';
import * as Oidc from 'oidc-client';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private settings: Oidc.UserManagerSettings;
  private manager: Oidc.UserManager;
  usuarioId: number;
  emisorId: number;
  token: string;

  constructor() {
    this.settings = {
      authority: environment.identityServer,
      client_id: environment.identityClient,
      redirect_uri: environment.facturadorUrl + 'login',
			response_type: 'code id_token token',
			scope: 'openid profile facturadorprofile APINegocios offline_access',
			post_logout_redirect_uri: environment.facturadorUrl + "logout",
			revokeAccessTokenOnSignout: true,
			userStore: new Oidc.WebStorageStateStore({ store: window.localStorage })
    };
    this.manager = new Oidc.UserManager(this.settings);
  }

  isLogged() {
    return this.usuarioId != null;
  }

  redirectToLogin() {
    return this.manager.signinRedirect();
  }

  validarLogin() {
    return this.manager.signinRedirectCallback()
      .then(user => {
        if (user) {
          this.usuarioId = user.profile.usuarioid;
          this.emisorId = user.profile.emisorid;
          this.token = user.access_token;
        } else {
          this.redirectToLogin();
        }
      });
  }

  obtenerUsuario() {
    return 208;
  }
}
