import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient, private session: SessionService) { }

  get headers(): HttpHeaders {
    return new HttpHeaders().set('Authorization', `Bearer ${this.session.token}`);
  }

  obtener<T>(urlBase: string, endpoint: string) {
    return this.httpClient.get<T>(urlBase + endpoint, { headers: this.headers });
  }

  post<T>(urlBase: string, endpoint: string, cuerpo: any) {
    return this.httpClient.post<T>(urlBase + endpoint, cuerpo, { headers: this.headers });
  }
}
