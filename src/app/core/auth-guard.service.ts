import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private session: SessionService) { }

  canActivate() {
    var isValid = this.session.isLogged();
    if (!isValid) {
      this.session.redirectToLogin();
    }
    
    return isValid;
  }
}
